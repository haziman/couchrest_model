# -*- encoding: utf-8 -*-
# stub: couchrest_model 2.2.0.beta1 ruby lib

Gem::Specification.new do |s|
  s.name = "couchrest_model".freeze
  s.version = "2.2.0.beta1"

  s.required_rubygems_version = Gem::Requirement.new("> 1.3.1".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["J. Chris Anderson".freeze, "Matt Aimonetti".freeze, "Marcos Tapajos".freeze, "Will Leinweber".freeze, "Sam Lown".freeze]
  s.date = "2017-10-25"
  s.description = "CouchRest Model provides aditional features to the standard CouchRest Document class such as properties, view designs, associations, callbacks, typecasting and validations.".freeze
  s.email = "jchris@apache.org".freeze
  s.extra_rdoc_files = ["LICENSE".freeze, "README.md".freeze, "THANKS.md".freeze]
  s.files = [".gitignore".freeze, ".rspec".freeze, ".travis.yml".freeze, "Dockerfile".freeze, "Gemfile".freeze, "Gemfile.activesupport-4.x".freeze, "Gemfile.activesupport-5.x".freeze, "Guardfile".freeze, "LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "THANKS.md".freeze, "VERSION".freeze, "benchmarks/Gemfile".freeze, "benchmarks/connections.rb".freeze, "benchmarks/dirty.rb".freeze, "couchrest_model.gemspec".freeze, "docker-compose.yml".freeze, "history.md".freeze, "init.rb".freeze, "lib/couchrest/model.rb".freeze, "lib/couchrest/model/associations.rb".freeze, "lib/couchrest/model/base.rb".freeze, "lib/couchrest/model/callbacks.rb".freeze, "lib/couchrest/model/casted_array.rb".freeze, "lib/couchrest/model/casted_by.rb".freeze, "lib/couchrest/model/configuration.rb".freeze, "lib/couchrest/model/connection.rb".freeze, "lib/couchrest/model/connection_config.rb".freeze, "lib/couchrest/model/core_extensions/hash.rb".freeze, "lib/couchrest/model/core_extensions/time_parsing.rb".freeze, "lib/couchrest/model/design.rb".freeze, "lib/couchrest/model/designs.rb".freeze, "lib/couchrest/model/designs/design_mapper.rb".freeze, "lib/couchrest/model/designs/migrations.rb".freeze, "lib/couchrest/model/designs/view.rb".freeze, "lib/couchrest/model/dirty.rb".freeze, "lib/couchrest/model/document_queries.rb".freeze, "lib/couchrest/model/embeddable.rb".freeze, "lib/couchrest/model/errors.rb".freeze, "lib/couchrest/model/extended_attachments.rb".freeze, "lib/couchrest/model/persistence.rb".freeze, "lib/couchrest/model/properties.rb".freeze, "lib/couchrest/model/property.rb".freeze, "lib/couchrest/model/property_protection.rb".freeze, "lib/couchrest/model/proxyable.rb".freeze, "lib/couchrest/model/server_pool.rb".freeze, "lib/couchrest/model/support/couchrest_database.rb".freeze, "lib/couchrest/model/translation.rb".freeze, "lib/couchrest/model/typecast.rb".freeze, "lib/couchrest/model/utils/migrate.rb".freeze, "lib/couchrest/model/validations.rb".freeze, "lib/couchrest/model/validations/casted_model.rb".freeze, "lib/couchrest/model/validations/locale/en.yml".freeze, "lib/couchrest/model/validations/uniqueness.rb".freeze, "lib/couchrest/railtie.rb".freeze, "lib/couchrest_model.rb".freeze, "lib/rails/generators/couchrest_model.rb".freeze, "lib/rails/generators/couchrest_model/config/config_generator.rb".freeze, "lib/rails/generators/couchrest_model/config/templates/couchdb.yml".freeze, "lib/rails/generators/couchrest_model/model/model_generator.rb".freeze, "lib/rails/generators/couchrest_model/model/templates/model.rb".freeze, "lib/tasks/migrations.rake".freeze, "spec/.gitignore".freeze, "spec/fixtures/attachments/README".freeze, "spec/fixtures/attachments/couchdb.png".freeze, "spec/fixtures/attachments/test.html".freeze, "spec/fixtures/config/couchdb.yml".freeze, "spec/fixtures/models/article.rb".freeze, "spec/fixtures/models/base.rb".freeze, "spec/fixtures/models/card.rb".freeze, "spec/fixtures/models/cat.rb".freeze, "spec/fixtures/models/client.rb".freeze, "spec/fixtures/models/course.rb".freeze, "spec/fixtures/models/designs.rb".freeze, "spec/fixtures/models/event.rb".freeze, "spec/fixtures/models/invoice.rb".freeze, "spec/fixtures/models/key_chain.rb".freeze, "spec/fixtures/models/membership.rb".freeze, "spec/fixtures/models/money.rb".freeze, "spec/fixtures/models/person.rb".freeze, "spec/fixtures/models/project.rb".freeze, "spec/fixtures/models/question.rb".freeze, "spec/fixtures/models/sale_entry.rb".freeze, "spec/fixtures/models/sale_invoice.rb".freeze, "spec/fixtures/models/service.rb".freeze, "spec/fixtures/models/user.rb".freeze, "spec/fixtures/views/lib.js".freeze, "spec/fixtures/views/test_view/lib.js".freeze, "spec/fixtures/views/test_view/only-map.js".freeze, "spec/fixtures/views/test_view/test-map.js".freeze, "spec/fixtures/views/test_view/test-reduce.js".freeze, "spec/functional/validations_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/unit/active_model_lint_spec.rb".freeze, "spec/unit/assocations_spec.rb".freeze, "spec/unit/attachment_spec.rb".freeze, "spec/unit/base_spec.rb".freeze, "spec/unit/casted_array_spec.rb".freeze, "spec/unit/casted_spec.rb".freeze, "spec/unit/configuration_spec.rb".freeze, "spec/unit/connection_config_spec.rb".freeze, "spec/unit/connection_spec.rb".freeze, "spec/unit/core_extensions/time_parsing_spec.rb".freeze, "spec/unit/design_spec.rb".freeze, "spec/unit/designs/design_mapper_spec.rb".freeze, "spec/unit/designs/migrations_spec.rb".freeze, "spec/unit/designs/view_spec.rb".freeze, "spec/unit/designs_spec.rb".freeze, "spec/unit/dirty_spec.rb".freeze, "spec/unit/embeddable_spec.rb".freeze, "spec/unit/inherited_spec.rb".freeze, "spec/unit/persistence_spec.rb".freeze, "spec/unit/properties_spec.rb".freeze, "spec/unit/property_protection_spec.rb".freeze, "spec/unit/property_spec.rb".freeze, "spec/unit/proxyable_spec.rb".freeze, "spec/unit/server_pool_spec.rb".freeze, "spec/unit/subclass_spec.rb".freeze, "spec/unit/support/couchrest_database_spec.rb".freeze, "spec/unit/translations_spec.rb".freeze, "spec/unit/typecast_spec.rb".freeze, "spec/unit/utils/migrate_spec.rb".freeze, "spec/unit/validations_spec.rb".freeze]
  s.homepage = "http://github.com/couchrest/couchrest_model".freeze
  s.licenses = ["Apache License 2.0".freeze]
  s.rubygems_version = "2.5.2".freeze
  s.summary = "Extends the CouchRest Document class for advanced modelling.".freeze
  s.test_files = ["spec/fixtures/attachments/README".freeze, "spec/fixtures/attachments/couchdb.png".freeze, "spec/fixtures/attachments/test.html".freeze, "spec/fixtures/config/couchdb.yml".freeze, "spec/fixtures/models/article.rb".freeze, "spec/fixtures/models/base.rb".freeze, "spec/fixtures/models/card.rb".freeze, "spec/fixtures/models/cat.rb".freeze, "spec/fixtures/models/client.rb".freeze, "spec/fixtures/models/course.rb".freeze, "spec/fixtures/models/designs.rb".freeze, "spec/fixtures/models/event.rb".freeze, "spec/fixtures/models/invoice.rb".freeze, "spec/fixtures/models/key_chain.rb".freeze, "spec/fixtures/models/membership.rb".freeze, "spec/fixtures/models/money.rb".freeze, "spec/fixtures/models/person.rb".freeze, "spec/fixtures/models/project.rb".freeze, "spec/fixtures/models/question.rb".freeze, "spec/fixtures/models/sale_entry.rb".freeze, "spec/fixtures/models/sale_invoice.rb".freeze, "spec/fixtures/models/service.rb".freeze, "spec/fixtures/models/user.rb".freeze, "spec/fixtures/views/lib.js".freeze, "spec/fixtures/views/test_view/lib.js".freeze, "spec/fixtures/views/test_view/only-map.js".freeze, "spec/fixtures/views/test_view/test-map.js".freeze, "spec/fixtures/views/test_view/test-reduce.js".freeze, "spec/functional/validations_spec.rb".freeze, "spec/spec_helper.rb".freeze, "spec/unit/active_model_lint_spec.rb".freeze, "spec/unit/assocations_spec.rb".freeze, "spec/unit/attachment_spec.rb".freeze, "spec/unit/base_spec.rb".freeze, "spec/unit/casted_array_spec.rb".freeze, "spec/unit/casted_spec.rb".freeze, "spec/unit/configuration_spec.rb".freeze, "spec/unit/connection_config_spec.rb".freeze, "spec/unit/connection_spec.rb".freeze, "spec/unit/core_extensions/time_parsing_spec.rb".freeze, "spec/unit/design_spec.rb".freeze, "spec/unit/designs/design_mapper_spec.rb".freeze, "spec/unit/designs/migrations_spec.rb".freeze, "spec/unit/designs/view_spec.rb".freeze, "spec/unit/designs_spec.rb".freeze, "spec/unit/dirty_spec.rb".freeze, "spec/unit/embeddable_spec.rb".freeze, "spec/unit/inherited_spec.rb".freeze, "spec/unit/persistence_spec.rb".freeze, "spec/unit/properties_spec.rb".freeze, "spec/unit/property_protection_spec.rb".freeze, "spec/unit/property_spec.rb".freeze, "spec/unit/proxyable_spec.rb".freeze, "spec/unit/server_pool_spec.rb".freeze, "spec/unit/subclass_spec.rb".freeze, "spec/unit/support/couchrest_database_spec.rb".freeze, "spec/unit/translations_spec.rb".freeze, "spec/unit/typecast_spec.rb".freeze, "spec/unit/utils/migrate_spec.rb".freeze, "spec/unit/validations_spec.rb".freeze]

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<couchrest>.freeze, ["= 2.0.1"])
      s.add_runtime_dependency(%q<activemodel>.freeze, [">= 4.0.2"])
      s.add_runtime_dependency(%q<tzinfo>.freeze, [">= 0.3.22"])
      s.add_runtime_dependency(%q<hashdiff>.freeze, ["~> 0.3"])
      s.add_development_dependency(%q<rspec>.freeze, ["~> 3.5.0"])
      s.add_development_dependency(%q<rack-test>.freeze, [">= 0.5.7"])
      s.add_development_dependency(%q<rake>.freeze, ["< 11.0", ">= 0.8.0"])
      s.add_development_dependency(%q<test-unit>.freeze, [">= 0"])
      s.add_development_dependency(%q<minitest>.freeze, ["> 4.1"])
      s.add_development_dependency(%q<kaminari>.freeze, ["< 0.16.0", ">= 0.14.1"])
      s.add_development_dependency(%q<mime-types>.freeze, ["< 3.0"])
    else
      s.add_dependency(%q<couchrest>.freeze, ["= 2.0.1"])
      s.add_dependency(%q<activemodel>.freeze, [">= 4.0.2"])
      s.add_dependency(%q<tzinfo>.freeze, [">= 0.3.22"])
      s.add_dependency(%q<hashdiff>.freeze, ["~> 0.3"])
      s.add_dependency(%q<rspec>.freeze, ["~> 3.5.0"])
      s.add_dependency(%q<rack-test>.freeze, [">= 0.5.7"])
      s.add_dependency(%q<rake>.freeze, ["< 11.0", ">= 0.8.0"])
      s.add_dependency(%q<test-unit>.freeze, [">= 0"])
      s.add_dependency(%q<minitest>.freeze, ["> 4.1"])
      s.add_dependency(%q<kaminari>.freeze, ["< 0.16.0", ">= 0.14.1"])
      s.add_dependency(%q<mime-types>.freeze, ["< 3.0"])
    end
  else
    s.add_dependency(%q<couchrest>.freeze, ["= 2.0.1"])
    s.add_dependency(%q<activemodel>.freeze, [">= 4.0.2"])
    s.add_dependency(%q<tzinfo>.freeze, [">= 0.3.22"])
    s.add_dependency(%q<hashdiff>.freeze, ["~> 0.3"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.5.0"])
    s.add_dependency(%q<rack-test>.freeze, [">= 0.5.7"])
    s.add_dependency(%q<rake>.freeze, ["< 11.0", ">= 0.8.0"])
    s.add_dependency(%q<test-unit>.freeze, [">= 0"])
    s.add_dependency(%q<minitest>.freeze, ["> 4.1"])
    s.add_dependency(%q<kaminari>.freeze, ["< 0.16.0", ">= 0.14.1"])
    s.add_dependency(%q<mime-types>.freeze, ["< 3.0"])
  end
end
